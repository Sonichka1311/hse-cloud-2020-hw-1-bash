#!/bin/bash

read -r one
read -r two
read -r three

if [[ ($one == $two) && ($two == $three) ]] 
then 
	echo -n EQUILATERAL
elif [[ ($one == $two) || ($two == $three) || ($three == $one) ]]
then
	echo -n ISOSCELES
else 
	echo -n SCALENE
fi
